"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
from flask_restful import Resource, Api, abort   # api interaction
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient # mongodb connection
import logging


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

api = Api(app)

# client = MongoClient("mongodb://localhost:27017")
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017) # <<<<<<<<<<<<<<<<< REMEMBER TO SWITCH
db = client.times



########
# Pages
########

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

def make_arrow(date: str, time: str) -> "Arrow":
    year = int(date[:4])
    month = int(date[5:7])
    day = int(date[8:])
    hour = int(time[:2])
    minute = int(time[3:])
    return arrow.Arrow(year, month, day, hour, minute)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))
    # get input brevet distance for race
    distance = request.args.get("brevet", type=str)
    # remove "km" off string ("200km" -> 200) and turn into int
    distance = int(distance[:-2])
    app.logger.debug("distance={}".format(distance))
    # get the input km for the race
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    race_date = request.args.get("date", type=str)
    app.logger.debug("race start date={}".format(race_date))
    race_time = request.args.get("time", type=str)
    app.logger.debug("race start time={}".format(race_time))
    start_race = make_arrow(race_date, race_time)
    app.logger.debug("iso-format race start={}".format(start_race.isoformat()))
    open_time = acp_times.open_time(km, distance, start_race)
    close_time = acp_times.close_time(km, distance, start_race)
    result = {"open": open_time, "close": close_time}
    app.logger.debug(f"times: {result}")
    return flask.jsonify(result=result)


######################
# MongoDB Interaction
######################

@app.route("/_store")
def _save_times():
    raw_times = request.args.to_dict()
    app.logger.debug(f"times={raw_times}")
    # calculate num times to insert
    num_times = len(raw_times)/2
    # don't insert empty time data
    if num_times > 0:
        times = {"open": [], "close": []}
        for time in raw_times:
            if raw_times[time] == "open":
                times["open"].append(time)
            else:
                times["close"].append(time)
        # remove old times
        db.times.remove({})
        # add new time
        db.times.insert_one(times)
    # send number of open/close times inserted
    return flask.jsonify(result={"ok": num_times})


@app.route("/times")
def display_times():
    _items = db.times.find()
    item = next(_items)
    num_times = len(item["open"])
    return flask.render_template("times.html", times=item, num_times=num_times)



########################
# CREATE TIME RESOURCE
########################

def make_csv_line(data: list) -> str:
    line = ""
    # add each item with a ',' at the end
    for item in data:
        line += str(item) + ','
    # replace trailing ',' with newline char
    if len(line) > 0:
        line = line[:-1] + '\n'
    return line

class Times(Resource):

    def __init__(self):
        self.valid_groups = ["listAll", "listOpenOnly", "listCloseOnly"]
        self.valid_types = ["csv", "json"]

    def __get_data(self, group: str) -> dict:
        """Gets entries from mongodb and return desired data"""
        # get times from database
        # select only open times
        if group == "listOpenOnly":
            _items = db.times.find({}, {"open": 1})
        # select only close times
        elif group == "listCloseOnly":
            _items = db.times.find({}, {"close": 1})
        # select all times
        else:
            _items = db.times.find()
        times = next(_items)
        # remove id class from dict
        times.pop("_id")
        return times

    def __limit_data(self, times: dict, limit: int) -> dict:
        """Limit number of times in dict to a selected number"""
        # don't alter data when no limit
        if limit is not None:
            # get time category (e.g. open)
            labels = list(times.keys())
            # shrink times to limit
            for label in labels:
                # only shrink when limit is smaller than length
                num_ts = len(times[label])
                if limit < num_ts:
                    times[label] = times[label][:limit]
        return times

    def __convert_csv(self, times: dict) -> str:
        """Create a csv string from a dictionary"""
        # get desired keys
        labels = list(times.keys())
        # create labels on first line
        csv_data = make_csv_line(labels)
        # add each line of time data
        num_times = len(times[labels[0]])
        for ti in range(num_times):
            line_data = []
            for label in labels:
                line_data.append(times[label][ti])
            time_line = make_csv_line(line_data)
            csv_data += time_line
        return csv_data

    def get(self, group="listAll", dtype="json"):
        # check if valid info is requested
        if group not in self.valid_groups:
            abort(404, msg="invalid dataset requested")
        if dtype not in self.valid_types:
            abort(404, msg="invalid return type requested")
        # create default limit
        limit = None
        # change limit if one was given
        uri_data = dict(request.args)
        if "top" in uri_data:
            try:
                limit = int(uri_data["top"])
            except ValueError:
                abort(404, msg="limit must be of type int")
        # get desired data
        times = self.__get_data(group)
        times = self.__limit_data(times, limit)
        # return data in desired format
        if dtype == "csv":
            return self.__convert_csv(times)
        else:
            return flask.jsonify(times)


# Create routes
# Another way, without decorators
api.add_resource(Times, '/', '/<group>', '/<group>/<dtype>')  # <<<<<<<<<<<<<< FINISH THIS!!!!



#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
